from pathlib import Path
from random import randint
from typing import Optional


class BTreeNode:
    """Representation of a tree node.
    
    This tree node get a value as an int and it's children.
    Because this note is used in a binary tree the note can only have two
    childs: *Left* and *Right*.
    """

    def __init__(self, value: int) -> None:
        self._value = value
        self._left = None
        self._right = None

    @property
    def value(self) -> int:
        """Return the value of the node."""
        return self._value

    def add_node_from_value(self, value: int) -> None:
        """Insert the node at the right place based on its value."""
        if self._value > value:
            if self._left:
                self._left.add_node_from_value(value)
            else:
                self._left = BTreeNode(value)
        elif self._value < value:
            if self._right:
                self._right.add_node_from_value(value)
            else:
                self._right = BTreeNode(value)

    def generate_dot_sequence(self, f) -> None:
        """Insert elements for the current node.
        
        Two line are created:

        - current -> left
        - current -> right
        """
        if self._left:
            f.write(f'{self.value} -> {self._left.value}\n')
            self._left.generate_dot_sequence(f)
        if self._right:
            f.write(f'{self.value} -> {self._right.value}\n')
            self._right.generate_dot_sequence(f)


class BTree:
    def __init__(self, root: Optional[BTreeNode] = None):
        self._root = root

    def add_node_from_value(self, value: int) -> None:
        """Add new node to the tree.
        
        If the _root node is None it's assigned to the new BTreeNode created
        from the value received otherwise the add_node_from_value from the
        _root node is called to insert the node at the right place.
        """
        if self._root is None:  # Clearer than if not self._root
            self._root = BTreeNode(value)
        else:
            self._root.add_node_from_value(value)

    def generate_dot_file(self, file_name, digraph_name: str = 'g') -> None:
        """Generate dot file that can be used with graphviz."""
        p = Path('/tmp') / file_name
        with p.open(mode='w+') as f:
            f.write(f'digraph {digraph_name} {{\n')
            self._root.generate_dot_sequence(f)
            f.write(f'}}\n')


nums = {randint(0, 1000000) for _ in range(2000)}
tree = BTree()
for num in nums:
    tree.add_node_from_value(num)

tree.generate_dot_file('graphviz.dot')
