e01 = {'id': 1, 'name': 'element_01'}
e02 = {'id': 2, 'name': 'element_02'}
e03 = {'id': 3, 'name': 'element_03'}

l1 = [e01, e02, e03]
l2 = [e03, e01, e02]

print(sorted(l1, key=lambda x: x['id']) == sorted(l2, key=lambda x: x['id']))
